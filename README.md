## Tasker Front-End

This is the Front-End for Tasker-App. 
It is written in React.JS, using Material-UI for the landing Page and Ant-Design on the Dashboard.

## What does the app do?

- Actually, you can login into the dashboard (obiviously you'll need an account otherwise you will not be able to login) and see a brief profile page.
- Authorization, login and logout calls to BackEnd are fully working. 
- Basic Dashboard Layout

## What needs to be done?

Well, lot of things, like: 

* Complete Profile Page
* Complete Dashboard Page
* Complete other sections of the App ( Tasks / Bugs, Team,  Repository, Developers)
* Custom ant-d theme

Please keep in mind that this application is developed only by Me, including Back-End and Database.

If you want to help me, drop me a message and we'll talk about it.


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
