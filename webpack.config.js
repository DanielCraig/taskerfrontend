const path = require('path');
const fs = require('fs');
const lessToJs = require('less-vars-to-js');
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, './ant-vars.less'), 'utf8'));

module.exports = {
  context: __dirname,
  entry: './src/App.js',
  mode: 'development',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, './')
  },
  resolve: {
    modules: ['node_modules']
  },
  module: {
    rules: [
      {
        loader: 'babel-loader',
        test: /\.js$/,
        options: {
          presets: [
            ['@babel/env', { modules: false, targets: { browsers: ['last 2 versions'] } }],
            '@babel/react',
          ],
          cacheDirectory: true,
          plugins: [
            ['import', { libraryName: "antd", style: true }],
            'transform-strict-mode',
            'transform-object-rest-spread',
            '@babel/plugin-proposal-class-properties',
          ]
        },
      },
      {
        test: /\.(css|less)$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          {
            loader: "less-loader",
            options: {
              javascriptEnabled: true,
              modifyVars: themeVariables,
              root: path.resolve(__dirname, './src')
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
    ]
  },
};