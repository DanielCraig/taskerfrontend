import React, { useState } from 'react'
import {
  Typography,
  IconButton,
  Container,
  Paper,
  TextField,
  Checkbox,
  FormControlLabel,
  Button,
  CircularProgress
} from '@material-ui/core'
import md5 from 'md5'
import axios from 'axios'
import Cookies from 'universal-cookie';
import { BrowserRouter as Link } from "react-router-dom";
import { setUserInfo, setJustLoggedIn, setLoginError, setProfilePictureUrl } from './../../store/actions.js'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Icon } from '@iconify/react'
import facebookSquare from '@iconify/icons-jam/facebook-square'
import arrowBackOutline from '@iconify/icons-ion/arrow-back-outline'
import instagramIcon from '@iconify/icons-fa/instagram'
import twitterIcon from '@iconify/icons-logos/twitter'
import Background from '../../assets/loginBG.jpg'
import { goToDashboard, goBack } from './navigation'

const Authentication = (props) => {

  const [rememberMe, setRememberMe] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const {
    history,
    setLoggedIn,
    setUser,
    setErrors,
    errorString,
    formHasErrors,
    setPictureUrl,
  } = props;


  //Function that will trigger when user submits the form.
  const handleLogin = () => {

    //Check email and password inputs
    if (email.length > 6 && password.length > 4) {

      //Set Loader
      setLoading(true);

      //Here we call the backend for the authentication process
      fetch('http://95.216.190.224:3000/auth/local/signin', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: email, password: password })
      }).then(data => data.json())
        .then(async res => {
          if (!res.error) {
            const exampleMail = 'ale.camplese@gmail.com';
            const hashedMail = md5(exampleMail.toLowerCase().trim());
            const gravatarUserProfile = await axios.get(`https://cors-anywhere.herokuapp.com/https://www.gravatar.com/${hashedMail}.json`);
            setPictureUrl(gravatarUserProfile.data.entry[0].thumbnailUrl)
            //Assign cookies and navigate to dashboard
            const cookies = new Cookies();
            cookies.set('id', res.id)
            cookies.set('expiry', res.expiry)
            const username = res.name + ' ' + res.surname
            setUser(username, res.id);
            setLoggedIn(false);
            goToDashboard(history)
          } else {
            setLoading(false)
            setErrors(true)
          }
        }).catch(err => {
          console.log(err)
        })
    }
  }

  return (
    <div style={style.authContainer}>
      <IconButton
        onClick={() => goBack(history)}
        disableRipple
        disableFocusRipple
        size="medium"
        style={style.backButton}
        color="inherit">
        <Icon
          width="0.9em"
          icon={arrowBackOutline}
          color="red"
        />
      </IconButton>
      <Paper
        style={style.loginPaper}
        elevation={20}>
        <Typography
          variant="h5"
          style={{ marginTop: '20px', marginBottom: '20px' }}>
          Login
        </Typography>
        <TextField
          helperText={email === "" ? 'Please write your email!' : ' '}
          error={email === ""}
          value={email}
          onChange={event => setEmail(event.target.value)}
          style={{ width: '250px' }}
          required
          variant="outlined"
          label="E-mail"
          margin="dense"
        />
        <TextField
          style={{ width: '250px' }}
          required
          helperText={password === "" ? 'Please write your password!' : ' '}
          error={password === ""}
          value={password}
          onChange={event => setPassword(event.target.value)}
          variant="outlined"
          label="Password"
          margin="dense"
        />
        <Container style={style.bottomFormContainer}>
          <FormControlLabel
            style={style.rememberCheckbox}
            control={
              <Checkbox
                checked={rememberMe}
                onChange={() => setRememberMe(true)}
                value="rememberMe"
                inputProps={{
                  'aria-label': 'primary checkbox',
                }}
              />
            }
            label="Ricordami"
          />
          {loading === false ?
            (<>
              <Button
                onClick={() => handleLogin()}
                variant="contained"
                color="secondary"
                style={style.loginButton}> Login
              </Button>
              {formHasErrors === true ? (
                <Container>
                  <Typography style={{ color: 'red', fontSize: 15, marginBottom: 20 }}> {errorString} </Typography>
                </Container>
              ) : null}
            </>
            ) :
            (<CircularProgress
              style={style.loader}
              color="secondary" />
            )}
          <Typography> oppure entra con i tuoi social preferiti:</Typography>
          <Container style={style.socialContainer}>
            <IconButton
              color="inherit">
              <Icon
                width="0.9em"
                icon={facebookSquare}
                color="#3d5b99"
              />
            </IconButton>
            <IconButton
              color="inherit">
              <Icon
                width="0.9em"
                icon={instagramIcon}
                color="#c13088"
              />
            </IconButton>
            <IconButton
              color="inherit">
              <Icon
                width="0.9em"
                icon={twitterIcon}
              />
            </IconButton>
          </Container>
          <Container style={style.registerContainer}>
            <Typography
              style={{ fontSize: 14 }}> Altrimenti, se non hai un'account,
            <Link
                style={style.registerLink}
                to="/"> crealo
            </Link> in pochi secondi.
            </Typography>
          </Container>
        </Container>
      </Paper>
    </div >
  )
}

//Styles Section

const style = {
  authContainer: {
    display: 'flex',
    backgroundImage: `url(${Background})`,
    backgroundSize: 'auto',
    justifyContent: 'center',
  },
  loginPaper: {
    height: '90vh',
    marginTop: '100px',
    marginRight: '100px',
    marginLeft: '30px',
    marginBottom: '100px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderColor: 'purple',
  },
  loginButton: {
    marginBottom: '30px'
  },
  loader: {
    marginBottom: '30px'
  },
  socialContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '10px'
  },
  rememberCheckbox: {
    marginBottom: '20px'
  },
  bottomFormContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '20px'
  },
  backLink: {
    color: 'red',
    textDecoration: 'none'
  },
  backButton: {
    maxHeight: '60px',
    marginTop: '40px'
  },
  registerContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
    paddingLeft: '20px',
    paddingTop: '20px'
  },
  registerLink: {
    color: 'red',
    fontSize: '14px',
    textDecoration: 'none'
  },

}

const mapStateToProps = state => ({
  username: state.userInfo.username,
  id: state.userInfo.id,
  hasJustLoggedIn: state.hasLoggedIn,
  formHasErrors: state.formHasErrors,
  errorString: state.errorString,
  userPicture: state.userProfilePicture,

})

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    setLoggedIn: setJustLoggedIn,
    setUser: setUserInfo,
    setErrors: setLoginError,
    setPictureUrl: setProfilePictureUrl,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Authentication);