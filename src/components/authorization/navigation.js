import Cookies from 'universal-cookie';


export const userLogout = history => {
  const cookies = new Cookies()
  cookies.remove('id')
  cookies.remove('expiry')
  history.replace('/');
}

export const goToProfile = history => {
  history.push({
    pathname: '/profile',
  });
}

export const goToDashboard = history => {
  history.push({
    pathname: '/dashboard',
  });
}

export const goBack = history => {
  history.goBack();
}