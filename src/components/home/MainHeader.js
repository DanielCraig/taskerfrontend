import React from 'react'
import { Container } from '@material-ui/core'
import Particles from 'react-particles-js';
import Background from '../../assets/loginBG.jpg'

export const MainHeader = () => {
  return (
    <Container maxWidth={false} style={style.container}>
      <Particles
        height="500px"
        params={{
          fps_limit: 60,
          particles: {
            number: {
              value: 60
            },
            line_linked: {
              shadow: {
                enable: true,
                color: '#eb34ae',
                blur: 5
              }
            }
          }
        }}
      >
      </Particles>
    </Container>
  )
}

//Styles Section
const style = {
  container: {
    backgroundImage: `url(${Background})`,
    backgroundSize: 'cover',
    display: 'flex',
    flexDirection: 'column',
    maxHeight: '100vh',
  }
}
