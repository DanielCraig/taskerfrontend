import React from 'react'
import { BrowserRouter as Router, Link } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core'

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: theme.mixins.toolbar,
})


const NavBar = (props) => {

  const { classes } = props

  return (
    <>
      <AppBar style={style.navbar} className={classes.appBar}>
        <Toolbar>
          <Typography style={{ flex: 1, fontFamily: 'Alegreya', fontWeight: '600', color: 'black' }} variant="h5">
            Tasker
          </Typography>
          <Button size="medium" style={style.loginbutton} color="inherit">
            <Link style={{ textDecoration: 'none', color: 'black' }} to="/authentication"> Login </Link>
          </Button>
        </Toolbar>
      </AppBar>
    </>
  )
}

//Styles Section
const style = {
  navbar: {
    backgroundColor: 'white',
    width: '100%',
    height: '60px',
  },
  loginbutton: {
    fontFamily: 'Alegreya',
    fontWeight: 700,
    color: 'black'
  }
}

export default withStyles(styles)(NavBar)
