import React from 'react';
import { Link } from "react-router-dom";
import { AppBar, Button, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: 'white',
    height: '60px',
  },
  loginButton: {
    fontFamily: 'Alegreya',
    fontWeight: 700,
    color: 'black'
  },
}));

export const MainAppBar = () => {
  const classes = useStyles();

  return (
    <>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <Typography style={style.headerText} variant="h5">
            Tasker
          </Typography>
          <Button size="medium" className={classes.loginButton} color="inherit">
            <Link style={style.loginLink} to="/authentication"> Login </Link>
          </Button>
        </Toolbar>
      </AppBar >
    </>
  )
}

const style = {
  headerText: {
    flex: 1,
    fontFamily: 'Alegreya',
    fontWeight: '600',
    color: 'black'
  },
  loginLink: {
    textDecoration: 'none',
    color: 'black'
  },
}
