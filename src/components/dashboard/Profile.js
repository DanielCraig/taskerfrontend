import React from 'react';
import { connect } from 'react-redux';
import { Layout, Menu, Icon, Typography, Avatar, Dropdown, Input, DatePicker, Tooltip, Button, Upload } from 'antd';
import moment from 'moment';
import userMenu from '../usermenu/usermenu';
import "antd/dist/antd.css";
import { goToDashboard } from './../authorization/navigation';


const ProfilePage = (props) => {

  const { Header, Content, Sider } = Layout;
  const { Text } = Typography;
  const { SubMenu } = Menu;

  const { username, history, userPicture } = props

  const dateFormat = 'DD/MM/YYYY'

  return (
    <Layout style={style.mainContainer}>
      <Sider
        theme="light"
        breakpoint="lg"
        collapsedWidth={0}
      >
        <div className="logo" />
        <Menu theme="light" mode="inline">
          <Menu.Item onClick={() => goToDashboard(history)} key="1">
            <Icon type="pie-chart" />
            <span> Dashboard </span>
          </Menu.Item>
          <Menu.Item key="2">
            <Icon type="bug" />
            <span>Task & Bugs </span>
          </Menu.Item>
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="user" />
                <span>Developers</span>
              </span>
            }
          >
            <Menu.Item key="3">React</Menu.Item>
            <Menu.Item key="4">React-Native</Menu.Item>
            <Menu.Item key="5">Angular</Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="team" />
                <span>Team</span>
              </span>
            }
          >
            <Menu.Item key="6">Team React</Menu.Item>
            <Menu.Item key="8">Team Angular</Menu.Item>
          </SubMenu>
          <Menu.Item key="9">
            <Icon type="file" />
            <span>Repository</span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout style={style.layoutContainer}>
        <Header style={style.header}>
          <Dropdown overlay={userMenu} placement="bottomCenter">
            <Avatar style={style.headerAvatar} size={48} src={userPicture} />
          </Dropdown>
        </Header>
        <Content style={style.mainCard}>
          <Content style={style.userContent}>
            <Content style={style.centerForm}>
              <Content style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                <Avatar style={{ marginBottom: 20 }} size={96} src={userPicture} />
                <Text style={style.userNameText}> {username} </Text>
                <Upload>
                  <Button>
                    <Icon type="upload" /> Update profile image
                  </Button>
                </Upload>
              </Content>
              <Content style={style.generalSection}>
                <Text style={style.generalSectionHeader}>General Info</Text>
              </Content>
              <Content style={style.mainForm}>
                <Text style={style.text}> Email</Text>
                <Input suffix={
                  <Tooltip title="This is your contact mail.">
                    <Icon type="info-circle" style={{ color: 'rgba(0,0,0,.45)' }} />
                  </Tooltip>
                }
                  prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} style={style.input} size="default" value="test.test1@gmail.com" />
                <Text style={style.text}> Username </Text>
                <Input suffix={
                  <Tooltip title="This will be your displayed name across Tasker.">
                    <Icon type="info-circle" style={{ color: 'rgba(0,0,0,.45)' }} />
                  </Tooltip>
                }
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} style={style.input} size="default" value="Danielcraig" />
                <Text style={style.text}> Birthday </Text>
                <DatePicker style={{ paddingLeft: 20 }} defaultDate={() => moment(), dateFormat} format={dateFormat} />
              </Content>
            </Content>
            <Content style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', marginRight: 30 }}>
              <Button> Save </Button>
            </Content>
          </Content>
        </Content>
      </Layout>
    </Layout >
  )
};

//Styles Section

const style = {
  mainContainer: {
    minHeight: '100vh'
  },
  layoutContainer: {
    display: 'flex',
    flexDirection: 'column'
  },
  header: {
    background: '#ffffff',
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'flex-end'
  },
  headerAvatar: {
    marginTop: 10
  },
  iconStyle: {
    marginRight: 5,
  },
  mainCard: {
    margin: "20px 16px 0",
  },
  userContent: {
    padding: 24,
    background: "#d8e8e8",
    color: 'white',
    boxShadow: '4px 4px 7px 1px rgba(50, 50, 50, 0.16)',
    borderRadius: '6px',
    textAlign: 'justify'
  },
  centerForm: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  mainForm: {
    borderWidth: 1,
    borderColor: 'white',
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'center',
    maxWidth: '25%'
  },
  text: {
    color: 'black',
    fontSize: 14,
  },
  input: {
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 20
  },
  userNameText: {
    fontSize: 16,
    marginBottom: 20,
    fontWeight: 420,
    color: 'black',
  },
  generalSectionHeader: {
    color: 'black',
    fontSize: 24
  },
  generalSection: {
    marginTop: 30
  }

}

const mapStateToProps = state => ({
  username: state.userInfo.username,
  id: state.userInfo.id,
  hasJustLoggedIn: state.hasLoggedIn,
  userPicture: state.userProfilePicture,
})
export default connect(mapStateToProps)(ProfilePage);