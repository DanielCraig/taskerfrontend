import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { setJustLoggedIn } from './../../store/actions.js'
import { bindActionCreators } from 'redux';
import { Layout, Menu, Icon, Typography, message, Avatar, Dropdown, Progress } from 'antd';
import "antd/dist/antd.css";
import userMenu from '../usermenu/usermenu';

const Dashboard = props => {

  const { username, id, hasJustLoggedIn, setLoggedIn, userPicture, history } = props


  //Greet the user with a message
  useEffect(() => {
    if (hasJustLoggedIn === false) {
      message.success(`Welcome back, ${username}!`, 3);
      setLoggedIn(true)
    }
  }, [username, id]);

  const { Header, Content, Sider } = Layout;
  const { Text, Title } = Typography;
  const { SubMenu } = Menu;

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider
        theme="light"
        breakpoint="lg"
        collapsedWidth={0}
      >
        <div className="logo" />
        <Menu
          theme="light"
          defaultSelectedKeys={["1"]}
          mode="inline"
        >
          <Menu.Item
            key="1"
          >
            <Icon
              type="pie-chart"
            />
            <span> Dashboard </span>
          </Menu.Item>
          <Menu.Item
            key="2"
          >
            <Icon
              type="bug"
            />
            <span>Task & Bugs </span>
          </Menu.Item>
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="user" />
                <span>Developers</span>
              </span>
            }
          >
            <Menu.Item
              key="3"
            >React
            </Menu.Item>
            <Menu.Item
              key="4"
            >React-Native
            </Menu.Item>
            <Menu.Item
              key="5"
            >Angular
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="team" />
                <span>Team</span>
              </span>
            }
          >
            <Menu.Item
              key="6"
            >Team React
            </Menu.Item>
            <Menu.Item
              key="8"
            >Team Angular
            </Menu.Item>
          </SubMenu>
          <Menu.Item key="9">
            <Icon type="file" />
            <span>Repository</span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout style={{ display: 'flex', flexDirection: 'column' }}>
        <Header
          style={{ background: '#ffffff', display: 'flex', flexDirection: 'row', alignContent: 'center', justifyContent: 'flex-end' }}
        >
          <Dropdown
            overlay={userMenu(history)}
            placement="bottomCenter"
          >
            <Avatar
              style={{ marginTop: 10 }}
              size={48}
              src={userPicture}
            />
          </Dropdown>
        </Header>
        <Content style={{ margin: "20px 16px 0" }}>
          <Content
            style={{ padding: 24, background: "#d8e8e8", boxShadow: '4px 4px 7px 1px rgba(50, 50, 50, 0.16)', borderRadius: '6px' }}
          >
            <Title
              level={4}
              style={{ color: 'black', paddingBottom: 20 }}> Overview
            </Title>
            <Content style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center' }}>
              <Progress
                type="dashboard"
                percent={42}
              />
              <Text style={{ fontWeight: 700 }}> Project Completion </Text>
            </Content>
          </Content>
        </Content>
        <Content style={{ margin: "20px 16px 0" }}>
          <Content
            style={{ padding: 24, background: "#d8e8e8", boxShadow: '4px 4px 7px 1px rgba(50, 50, 50, 0.16)', borderRadius: '6px' }}>
            <Title
              level={4}
              style={{ color: 'black', paddingBottom: 20 }}> Overview
            </Title>
            <Content style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center' }}>
              <Text style={{ fontWeight: 700 }}> Project Completion </Text>
            </Content>
          </Content>
        </Content>
      </Layout>
    </Layout >
  )
};

const mapStateToProps = state => ({
  username: state.userInfo.username,
  id: state.userInfo.id,
  hasJustLoggedIn: state.hasLoggedIn,
  userPicture: state.userProfilePicture,
})

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    setLoggedIn: setJustLoggedIn
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);