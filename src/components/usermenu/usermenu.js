import { Menu, Icon } from 'antd';
import React from 'react';
import { userLogout, goToProfile } from './../authorization/navigation'

export const userMenu = history => (
    <Menu>
      <Menu.Item onClick={() => goToProfile(history)}>
        <Icon style={{ marginRight: 5 }} type="user" />
        Profile
      </Menu.Item>
      <Menu.Item onClick={() => userLogout(history)}>
        <Icon style={{ marginRight: 5 }} type="logout" />
        Logout
    </Menu.Item>
    </Menu>
  );

  export default userMenu