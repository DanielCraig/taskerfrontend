import { SET_USERINFO, SET_JUSTLOGGED, UPDATE_USERNAME, SET_LOGIN_HAS_ERROR, SET_PROFILE_PICTURE_URL } from './actions.js';

const initialState = {
  userInfo: {},
  hasLoggedIn: true,
  userName: '',
  formHasErrors: false,
  userProfilePicture: '',
  errorString: 'There was an issue while we were authenticating you, please check your credentials and try again.'
}

const getUserInfo = (state, action) => {
  return {
    ...state,
    userInfo: action.data
  }
}

const getUsername = (state, action) => {
  return {
    ...state,
    userName: action.updateUsername
  }
}

const getFormHasErrors = (state, action) => {
  return {
    ...state,
    formHasErrors: action.formHasErrors
  }
}

const getUserProfilePicture = (state, action) => {
  return {
    ...state,
    userProfilePicture: action.url
  }
}

const getLoggedIn = (state, action) => ({
  ...state,
  hasLoggedIn: action.payload
})

export function taskerUser(state = initialState, action) {
  switch (action.type) {
    case SET_USERINFO:
      return getUserInfo(state, action)
    case SET_JUSTLOGGED:
      return getLoggedIn(state, action)
    case UPDATE_USERNAME:
      return getUsername(state, action)
    case SET_LOGIN_HAS_ERROR:
      return getFormHasErrors(state, action)
    case SET_PROFILE_PICTURE_URL:
      return getUserProfilePicture(state, action)
    default:
      return state;
  }
}
