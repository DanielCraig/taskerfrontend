import { createStore, compose, applyMiddleware } from 'redux';
import { taskerUser } from './reducer'
import logger from 'redux-logger'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const MainStore = createStore(taskerUser, composeEnhancers(applyMiddleware(logger)));


