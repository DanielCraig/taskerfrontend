export const SET_USERINFO = 'SET_USERINFO';
export const SET_LOADER = 'SET_LOADER';
export const SET_JUSTLOGGED = 'SET_JUSTLOGGED';
export const UPDATE_USERNAME = 'UPDATE_USERNAME';
export const SET_LOGIN_HAS_ERROR = 'SET_LOGIN_HAS_ERROR';
export const SET_PROFILE_PICTURE_URL = 'SET_PROFILE_PICTURE_URL'


export function setUserInfo(username, userid) {
  return {
    type: SET_USERINFO,
    data: {
      username: username,
      id: userid,
    }
  }
};

export function setLoader(loader) {
  return { type: SET_LOADER, loader }
}

export function updateUsername(username) {
  return { type: UPDATE_USERNAME, username }
}

export function setLoginError(formHasErrors) {
  return { type: SET_LOGIN_HAS_ERROR, formHasErrors }
}

export function setProfilePictureUrl(url) {
  return { type: SET_PROFILE_PICTURE_URL, url };
}


export function setJustLoggedIn(justLoggedIn) {
  return {
    type: SET_JUSTLOGGED,
    payload: justLoggedIn,
  }
}