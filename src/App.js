import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from 'react-redux'
import { Home } from './components//home/Home.js'
import Authentication from './components/authorization/index'
import Dashboard from './components/dashboard/index'
import ProfilePage from './components/dashboard/Profile'
import { MainStore } from './store/store'

//Main App with router.

function App() {
  return (
    <Provider store={MainStore}>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/authentication" component={Authentication} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/profile" component={ProfilePage} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;